#pragma once

#ifdef LOCA_EXPORT
// DLL library project uses this
#define LOCA_ENTRY __declspec(dllexport)
#else
#ifdef LOCA_IMPORT
// client of DLL uses this
#define LOCA_ENTRY __declspec(dllimport)
#else
// static library project uses this
#define LOCA_ENTRY
#endif
#endif

// This warnings complains about std not having a dll interface. For more info see:
// http://www.unknownroad.com/rtfm/VisualStudio/warningC4251.html
#pragma warning( disable : 4251 )
