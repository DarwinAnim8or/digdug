#include "stdafx.h"
#include "TextLocalizer.h"
#include "csv.h"

void dae::TextLocalizer::Load(const std::string& filename, const std::string& locale) {
	m_strings.clear();

	io::CSVReader < 2, io::trim_chars<' '>, io::no_quote_escape < ';' >> in(filename);
	in.read_header(io::ignore_extra_column, "key", locale);
	std::string key, translation;
	while (in.read_row(key, translation)) {
		m_strings.insert(std::make_pair(key, translation));
	}
}

std::string dae::TextLocalizer::Get(const std::string& key) 
{
	auto it = m_strings.find(key);
	if (it != m_strings.end()) return it->second;
	return key;
}
