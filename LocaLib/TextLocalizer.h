#pragma once
#include <string>
#include <map>
#include "ITextLocalizer.h"
#include "LocaLibType.h"

namespace dae
{
	class TextLocalizer final : public ITextLocalizer
	{
	public:
		LOCA_ENTRY void Load(const std::string& filename, const std::string& locale) override;
		LOCA_ENTRY std::string Get(const std::string& key) override;

	private:
		std::map<std::string, std::string> m_strings;
	};
}
