#include "MiniginPCH.h"
#include "SceneManager.h"
#include "GameObject.h"

unsigned int dae::SceneManager::mNextObjectID = 0;

void dae::SceneManager::Update(float deltaTime)
{
	mObjectsMarkedForDelete.clear();

	for (auto obj : mObjects)
	{
		if (obj.second) obj.second->Update(deltaTime);
	}

	for (auto id : mObjectsMarkedForDelete)
	{
		for (auto obj : mObjects)
		{
			if (obj.first == id && obj.second)
			{
				delete obj.second;
				obj.second = nullptr;
				mObjects.erase(id);
				break;
			}
		}
	}
}

void dae::SceneManager::Render()
{
	for (auto obj : mObjects)
	{
		if (obj.second) obj.second->Render();
	}
}

dae::GameObject * dae::SceneManager::CreateObject()
{
	GameObject* obj = new GameObject(mNextObjectID++);
	mObjects.insert(std::make_pair(mNextObjectID - 1, obj));
	return obj;
}

dae::SceneManager::~SceneManager()
{
	DestroyAllObjects(); 
}

void dae::SceneManager::DestroyAllObjects()
{
	for (auto obj : mObjects)
	{
		if (obj.second)
		{
			delete obj.second;
			obj.second = nullptr;
		}
	}

	mObjects.clear();
}

void dae::SceneManager::DestroyObject(unsigned int objectID)
{
	mObjectsMarkedForDelete.push_back(objectID);
}

dae::GameObject * dae::SceneManager::GetGameObject(unsigned int objectID)
{
	for (auto obj : mObjects)
	{
		if (obj.first == objectID) return obj.second;
	}

	return nullptr;
}
