#pragma once
#include <string>
#include "Singleton.h"
#include "Common.h"
#include "TextLocalizer.h"

namespace dae
{
	class GameManager final : public Singleton<GameManager>
	{
	public:
		void Initialize();
		
		void LoadLanguage(const std::string& language);
		const std::string& GetCurrentLanguage() const {	return mCurrentLanguage; }

		void LoadMainMenu();
		void LoadLevel();

		void CheckInput();

		const GameState& GetGameState() const { return mGameState; }

	private:
		void LoadBaseLevel();
		void LoadFirstLevel();
		void LoadSecondLevel();

	private:
		TextLocalizer mTextLocalizer;
		std::string mCurrentLanguage;

		GameState mGameState;
		bool mHasCompletedLevelOne = false;
	};
}