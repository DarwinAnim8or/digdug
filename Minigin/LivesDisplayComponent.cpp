#include "MiniginPCH.h"
#include "LivesDisplayComponent.h"
#include "TextComponent.h"
#include "SceneManager.h"
#include "PlayerComponent.h"
#include "GameObject.h"

//Disabled here because the update is unused, the observer pattern will let us know when things change.
#pragma warning(push)
#pragma warning(disable: 4100)
void dae::LivesDisplayComponent::Update(float deltaTime)
{
}
#pragma warning(pop)

void dae::LivesDisplayComponent::RegisterComponent(BaseComponent * component)
{
	if (component->GetComponentID() == ComponentID::TextComponent)
		mTextComponent = static_cast<TextComponent*>(component);
}

void dae::LivesDisplayComponent::onNotify(unsigned int objectID, Event event)
{
	switch (event)
	{
	case Event::livesChanged: {
		UpdateLivesDisplay(objectID);
		break;
	}
	}
}

void dae::LivesDisplayComponent::UpdateLivesDisplay(unsigned int objectID)
{
	auto obj = SceneManager::GetInstance().GetGameObject(objectID);
	if (!obj) return;

	PlayerComponent* player = static_cast<PlayerComponent*>(obj->GetComponent(ComponentID::PlayerComponent));
	if (!player) return;

	mCurrentLives = player->GetLives();
	mTextComponent->SetText(std::to_string(mCurrentLives) + " Lives");
}
