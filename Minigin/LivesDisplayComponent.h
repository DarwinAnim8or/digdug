#pragma once
#include "Common.h"
#include "Observer.h"

namespace dae
{
	class TextComponent;

	class LivesDisplayComponent final : public BaseComponent, public Observer
	{
	public:
		LivesDisplayComponent() : BaseComponent(ComponentID::FPSComponent) {}
		void Update(float deltaTime);
		void RegisterComponent(BaseComponent* component);
		void onNotify(unsigned int objectID, Event event);

	private:
		void UpdateLivesDisplay(unsigned int objectID);

	private:
		TextComponent* mTextComponent;
		int mCurrentLives;
	};
}