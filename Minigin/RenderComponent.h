#pragma once
#include "Common.h"
#include "Texture2D.h"
#include <memory>
#include <string>

namespace dae
{
	class TransformComponent;

	class RenderComponent : public BaseComponent
	{
	public:
		RenderComponent() : BaseComponent(ComponentID::RenderComponent) {}
		void Render();
		void Update(float deltaTime);
		void SetTexture(const std::string& path);
		void RegisterComponent(BaseComponent* component);

	//private: //Commented out for now until I find a better solution.
		std::shared_ptr<Texture2D> mTexture;
		TransformComponent* mTransformComponent;
	};
}