#include "MiniginPCH.h"
#include "GameObject.h"
#include "ResourceManager.h"
#include "Renderer.h"
#include "RenderComponent.h"
#include "SceneManager.h"
#include "FPSComponent.h"

dae::GameObject::GameObject(unsigned int objectID) :
	mObjectID(objectID)
{
}

dae::GameObject::~GameObject()
{
	for (auto& p : mComponents)
	{
		if (p.second)
		{
			delete p.second;
			p.second = nullptr;
		}
	}

	mComponents.clear();
}

void dae::GameObject::Update(float deltaTime)
{
	for (auto& p : mComponents)
	{
		if (p.second)
		{
			p.second->Update(deltaTime);
		}
	}
}

void dae::GameObject::Render() const
{
	for (auto& p : mComponents)
	{
		if (p.first == ComponentID::RenderComponent && p.second)
		{
			auto comp = static_cast<RenderComponent*>(p.second);
			if (comp) comp->Render();
		}
	}
}

const unsigned int dae::GameObject::GetObjectID()
{
	return mObjectID;
}

void dae::GameObject::AddComponent(BaseComponent * component)
{
	mComponents.insert(std::make_pair(component->GetComponentID(), component));
}

dae::BaseComponent * dae::GameObject::GetComponent(ComponentID id)
{
	for (auto comp : mComponents)
		if (comp.first == id) return comp.second;

	return nullptr;
}
