#include "MiniginPCH.h"
#include "GameManager.h"
#include "ResourceManager.h"
#include "SceneManager.h"
#include "GameObject.h"
#include "BinaryReader.h"
#include "InputManager.h"

#include "TextComponent.h"
#include "RenderComponent.h"
#include "FPSComponent.h"
#include "TransformComponent.h"
#include "PlayerComponent.h"
#include "LivesDisplayComponent.h"
#include "AIComponent.h"

using namespace dae;

void dae::GameManager::Initialize()
{
	mTextLocalizer = TextLocalizer();
	LoadLanguage("en-us");

	mGameState = GameState::menu;
	mHasCompletedLevelOne = false;
}

void dae::GameManager::LoadLanguage(const std::string & language)
{
	mTextLocalizer.Load("loca.csv", language);
}

void dae::GameManager::LoadMainMenu()
{
	//Create our title:
	auto go = SceneManager::GetInstance().CreateObject();
	auto text = new TextComponent();
	auto ren = new RenderComponent();
	auto trans = new TransformComponent();

	go->AddComponent(text);
	go->AddComponent(ren);
	go->AddComponent(trans);
	text->RegisterComponent(ren);
	ren->RegisterComponent(trans);

	text->SetFont("Lingua.otf", 36);
	trans->SetPosition(250.0f, 20.0f);
	text->SetText("DigDug");

	//Create our text objects:
	go = SceneManager::GetInstance().CreateObject();
	text = new TextComponent();
	ren = new RenderComponent();
	trans = new TransformComponent();

	go->AddComponent(text);
	go->AddComponent(ren);
	go->AddComponent(trans);
	text->RegisterComponent(ren);
	ren->RegisterComponent(trans);

	text->SetFont("Lingua.otf", 24);
	trans->SetPosition(20.0f, 100.0f);
	text->SetText("Press A to start with one player.");

	go = SceneManager::GetInstance().CreateObject();
	text = new TextComponent();
	ren = new RenderComponent();
	trans = new TransformComponent();

	go->AddComponent(text);
	go->AddComponent(ren);
	go->AddComponent(trans);
	text->RegisterComponent(ren);
	ren->RegisterComponent(trans);

	text->SetFont("Lingua.otf", 24);
	trans->SetPosition(20.0f, 140.0f);
	text->SetText("Press X to quit (or Y to reset while playing)");
}

void dae::GameManager::LoadLevel()
{
	SceneManager::GetInstance().DestroyAllObjects();

	//auto reader = BinaryReader("./res/level1.lvl");
	LoadBaseLevel();
	LoadFirstLevel();

	//if (mHasCompletedLevelOne) LoadFirstLevel();
	//else LoadSecondLevel();

	//Create DigDug:
	auto go = SceneManager::GetInstance().CreateObject();
	auto ren = new RenderComponent();
	auto trans = new TransformComponent();
	auto player = new PlayerComponent(go->GetObjectID());

	go->AddComponent(ren);
	go->AddComponent(trans);
	go->AddComponent(player);
	ren->RegisterComponent(trans);
	ren->SetTexture("dd.png");
	player->RegisterComponent(ren);
	player->RegisterComponent(trans);
	trans->SetPosition(320.0f, 320.0f);

	//Create our FPS object:
	go = SceneManager::GetInstance().CreateObject();
	auto fps = new FPSComponent();
	auto text = new TextComponent();
	ren = new RenderComponent();
	trans = new TransformComponent();

	go->AddComponent(fps);
	go->AddComponent(text);
	go->AddComponent(ren);
	go->AddComponent(trans);
	fps->RegisterComponent(text);
	text->RegisterComponent(ren);
	ren->RegisterComponent(trans);

	text->SetFont("Lingua.otf", 24);

	//Create our health display object:
	go = SceneManager::GetInstance().CreateObject();
	auto lives = new LivesDisplayComponent();
	ren = new RenderComponent();
	trans = new TransformComponent();
	text = new TextComponent();

	go->AddComponent(lives);
	go->AddComponent(text);
	go->AddComponent(ren);
	go->AddComponent(trans);

	text->RegisterComponent(ren);
	lives->RegisterComponent(text);
	ren->RegisterComponent(trans);
	text->SetFont("Lingua.otf", 24);
	trans->SetPosition(280.0f, 0.0f);

	player->addObserver(lives);
	player->OnHit();

	mGameState = GameState::playing;
}

void dae::GameManager::CheckInput()
{
	if (InputManager::GetInstance().IsPressed(ControllerButton::ButtonA) && mGameState == GameState::menu)
		LoadLevel();
	else if (InputManager::GetInstance().IsPressed(ControllerButton::ButtonY) && mGameState == GameState::playing)
		LoadLevel();
}

void dae::GameManager::LoadBaseLevel()
{
	auto go = SceneManager::GetInstance().CreateObject();
	auto ren = new RenderComponent();
	auto trans = new TransformComponent();
	go->AddComponent(ren);
	go->AddComponent(trans);
	ren->RegisterComponent(trans);
	ren->SetTexture("background.png");
}

void dae::GameManager::LoadFirstLevel()
{
	//Create our test Pooka:
	auto go = SceneManager::GetInstance().CreateObject();
	auto ren = new RenderComponent();
	auto trans = new TransformComponent();
	auto ai = new AIComponent();
	go->AddComponent(ren);
	go->AddComponent(trans);
	go->AddComponent(ai);
	ren->RegisterComponent(trans);
	ren->SetTexture("pooka.png");
	ai->RegisterComponent(trans);
	trans->SetPosition(150.0f, 150.0f);
}

void dae::GameManager::LoadSecondLevel()
{
}
