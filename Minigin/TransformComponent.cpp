#include "MiniginPCH.h"
#include "TransformComponent.h"

using namespace dae;

#pragma warning(push)
#pragma warning(disable: 4100)
void dae::TransformComponent::Update(float deltaTime)
{
}

void dae::TransformComponent::RegisterComponent(BaseComponent * component)
{
}
#pragma warning(pop)

const dae::Transform & dae::TransformComponent::GetTransform()
{
	return mTransform;
}

void dae::TransformComponent::SetPosition(float x, float y)
{
	mTransform.SetPosition(x, y, 0.0f);
}
