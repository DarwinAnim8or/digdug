#pragma once
#include "Common.h"
#include "Subject.h"

namespace dae
{
	class RenderComponent;
	class TransformComponent;
	class MoveCommand;
	class FirePumpCommand;

	enum class PlayerState
	{
		alive,
		moving,
		stunned,
		dead
	};

	class PlayerComponent : public BaseComponent, public Subject
	{
	public:
		PlayerComponent(unsigned int objID) : mObjectID(objID), BaseComponent(ComponentID::PlayerComponent) {}
		~PlayerComponent();

		void Update(float deltaTime);
		void RegisterComponent(BaseComponent* component);
		void OnHit();

		const PlayerState& GetState() { return mState; }
		const int GetLives() { return mLives; }

	private:
		void CheckForInput();

	private:
		unsigned int mObjectID;
		const float mDistanceToMove = 5.0f;
		const float mSpeed = 50.0f;
		int mLives = 3;
		const float mStunTime = 0.5f;
		float mStateTimer = 0.0f;
		PlayerState mState;
		Direction mCurrentDirection;

		RenderComponent* mRenderComponent;
		TransformComponent* mTransformComponent;

		MoveCommand* mCurrentMove;
		FirePumpCommand* mPumpCmd;
	};
}