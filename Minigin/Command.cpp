#include "MiniginPCH.h"
#include "Command.h"
#include "TransformComponent.h"
#include "SceneManager.h"
#include "RenderComponent.h"
#include "GameObject.h"
#include "PumpComponent.h"

bool dae::MoveCommand::execute(float deltaTime)
{
	if (mIsCompleted) return true;

	auto& pos = mTrans->GetTransform().GetPosition();
	float x = pos.x;
	float y = pos.y;

	switch (mDir)
	{
	case Direction::up: 
	{
		if (!mHasRunOnce) 
		{
			mGoalX = x;
			mGoalY = y - mDistance;
		}

		y -= mVelocity * deltaTime;
		break;
	}

	case Direction::down:
	{
		if (!mHasRunOnce)
		{
			mGoalX = x;
			mGoalY = y + mDistance;
		}

		y += mVelocity * deltaTime;
		break;
	}

	case Direction::left:
	{
		if (!mHasRunOnce)
		{
			mGoalX = x - mDistance;
			mGoalY = y;
		}

		x -= mVelocity * deltaTime;
		break;
	}

	case Direction::right:
	{
		if (!mHasRunOnce)
		{
			mGoalX = x + mDistance;
			mGoalY = y;
		}

		x += mVelocity * deltaTime;
		break;
	}
	}

	mTrans->SetPosition(x, y);
	mHasRunOnce = true;

	if (int(x) == int(mGoalX) && int(y) == int(mGoalY))
		mIsCompleted = true;

	return mIsCompleted;
}

#pragma warning(push)
#pragma warning(disable: 4100)
bool dae::FirePumpCommand::execute(float deltaTime)
{
	auto go = SceneManager::GetInstance().CreateObject();
	auto ren = new RenderComponent();
	auto trans = new TransformComponent();
	auto pump = new PumpComponent(go->GetObjectID());

	go->AddComponent(ren);
	go->AddComponent(trans);
	go->AddComponent(pump);
	ren->RegisterComponent(trans);
	ren->SetTexture("pump.png");

	trans->SetPosition(mX, mY);
	pump->RegisterMoveCommand(new MoveCommand(mDir, trans, 80.0f, 40.0f));

	return true;
}
#pragma warning(pop)