#include "MiniginPCH.h"
#include "PlayerComponent.h"
#include "RenderComponent.h"
#include "TransformComponent.h"
#include "InputManager.h"
#include "Command.h"

dae::PlayerComponent::~PlayerComponent()
{
	if (mPumpCmd)
	{
		delete mPumpCmd;
		mPumpCmd = nullptr;
	}

	if (mCurrentMove) 
	{
		delete mCurrentMove;
		mCurrentMove = nullptr;
	}
}

void dae::PlayerComponent::Update(float deltaTime)
{
	if (mPumpCmd)
	{
		if (mPumpCmd->execute(deltaTime))
		{
			delete mPumpCmd;
			mPumpCmd = nullptr;
		}
		else return;
	}

	if (mCurrentMove)
	{
		if (mCurrentMove->execute(deltaTime))
		{
			delete mCurrentMove;
			mCurrentMove = nullptr;
		}
		else return;
	}

	if (mState == PlayerState::stunned && mStateTimer < mStunTime)
	{
		mStateTimer += deltaTime;
		if (mStateTimer > mStunTime)
		{
			mState = PlayerState::alive;
			mStateTimer = 0.0f;
		}
			
		return;
	}

	if (mState == PlayerState::alive) CheckForInput();
}

void dae::PlayerComponent::RegisterComponent(BaseComponent * component)
{
	if (component->GetComponentID() == ComponentID::RenderComponent)
		mRenderComponent = static_cast<RenderComponent*>(component);

	if (component->GetComponentID() == ComponentID::TransformComponent)
		mTransformComponent = static_cast<TransformComponent*>(component);
}

void dae::PlayerComponent::OnHit()
{
	mLives--;
	notify(mObjectID, Event::livesChanged);
	mTransformComponent->SetPosition(320.0f, 320.0f);
	mState = PlayerState::stunned;
}

void dae::PlayerComponent::CheckForInput()
{
	auto& input = InputManager::GetInstance();

	if (input.IsPressed(ControllerButton::ButtonA))
	{
		auto& pos = mTransformComponent->GetTransform().GetPosition();
		mPumpCmd = new FirePumpCommand(mCurrentDirection, pos.x, pos.y);
		mState = PlayerState::stunned;
		return;
	}

	if (input.IsPressed(ControllerButton::TrackUp)) 
	{
		mCurrentMove = new MoveCommand(Direction::up, mTransformComponent, mSpeed, mDistanceToMove);
		mCurrentDirection = Direction::up;
		return;
	}

	if (input.IsPressed(ControllerButton::TrackDown))
	{
		mCurrentMove = new MoveCommand(Direction::down, mTransformComponent, mSpeed, mDistanceToMove);
		mCurrentDirection = Direction::down;
		return;
	}

	if (input.IsPressed(ControllerButton::TrackLeft))
	{
		mCurrentMove = new MoveCommand(Direction::left, mTransformComponent, mSpeed, mDistanceToMove);
		mCurrentDirection = Direction::left;
		return;
	}
		
	if (input.IsPressed(ControllerButton::TrackRight))
	{
		mCurrentMove = new MoveCommand(Direction::right, mTransformComponent, mSpeed, mDistanceToMove);
		mCurrentDirection = Direction::right;
		return;
	}
}
