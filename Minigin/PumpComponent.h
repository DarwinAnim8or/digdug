#pragma once
#include "Common.h"

namespace dae
{
	class TransformComponent;
	class MoveCommand;

	class PumpComponent final : public BaseComponent
	{
	public:
		PumpComponent(unsigned int parentID) : mParentID(parentID), BaseComponent(ComponentID::PumpComponent) {}
		~PumpComponent();

		void Update(float deltaTime);
		void RegisterComponent(BaseComponent* component);
		void RegisterMoveCommand(MoveCommand* cmd) { mMove = cmd; }

	private:
		unsigned int mParentID;
		TransformComponent* mTrans;
		MoveCommand* mMove;
		const float mMaxTimeAlive = 5.0f;
		float mCurrentTimeAlive = 0.0f;
	};
}