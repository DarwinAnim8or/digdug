#pragma once
#include <string>
#include <fstream>

class BinaryReader {
public:
	BinaryReader(const std::string& outpath);
	~BinaryReader();

	template<typename T>
	void ReadVariable(T& value) {
		if (!m_Instream || !m_Instream->is_open()) return;
		m_Instream->read(reinterpret_cast<char*>(&value), sizeof(T));
	}

	std::string ReadString();

	const std::string& GetOutpath() const { return m_Outpath; }
	const bool GetIsOkay();
	const size_t GetFileSize() const { return m_FileSize; }
	const size_t GetCurrentPosition();
	void Seek(size_t newPosition);

private:
	std::string m_Outpath;
	std::ifstream* m_Instream;
	size_t m_FileSize;
};