#pragma once
#include "Common.h"

namespace dae
{
	class GameObject;
	class TransformComponent;
	class MoveCommand;

	class AIComponent : public BaseComponent
	{
	public:
		AIComponent() : BaseComponent(ComponentID::AIComponent) {}
		~AIComponent();

		void Update(float deltaTime);
		void RegisterComponent(BaseComponent* component);

	private:
		GameObject* mTarget;
		TransformComponent* mTrans;
		MoveCommand* mMove;
	};
}