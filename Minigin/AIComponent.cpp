#include "MiniginPCH.h"
#include "AIComponent.h"
#include "TransformComponent.h"
#include "SceneManager.h"
#include "GameObject.h"
#include "PlayerComponent.h"
#include "Command.h"

dae::AIComponent::~AIComponent()
{
	if (mMove)
	{
		delete mMove;
		mMove = nullptr;
	}
}

void dae::AIComponent::Update(float deltaTime)
{
	if (!mTarget)
	{
		auto objects = SceneManager::GetInstance().GetAllGameObjects();
		for (auto obj : objects)
		{
			if (obj.second && obj.second->GetComponent(ComponentID::PlayerComponent))
			{
				mTarget = obj.second;
				break;
			}
		}
	}
	else
	{
		if (mMove)
		{
			if (mMove->execute(deltaTime))
			{
				delete mMove;
				mMove = nullptr;
			}
		}
		else
		{
			auto tr = static_cast<TransformComponent*>(mTarget->GetComponent(ComponentID::TransformComponent));
			auto pos = tr->GetTransform().GetPosition();

			if (int(pos.y) == int(mTrans->GetTransform().GetPosition().y) && int(pos.x) == int(mTrans->GetTransform().GetPosition().x))
			{
				auto player = static_cast<PlayerComponent*>(mTarget->GetComponent(ComponentID::PlayerComponent));
				if (player)
				{
					player->OnHit();
					mTrans->SetPosition(150.0f, 150.0f);
				}
			}
			
			if (pos.y > mTrans->GetTransform().GetPosition().y)
			{
				mMove = new MoveCommand(Direction::down, mTrans, 50.0f, 10.0f);
				return;
			}

			if (pos.x > mTrans->GetTransform().GetPosition().x)
			{
				mMove = new MoveCommand(Direction::right, mTrans, 50.0f, 10.0f);
				return;
			}

			if (pos.x < mTrans->GetTransform().GetPosition().x)
			{
				mMove = new MoveCommand(Direction::left, mTrans, 50.0f, 10.0f);
				return;
			}

			if (pos.y < mTrans->GetTransform().GetPosition().y)
			{
				mMove = new MoveCommand(Direction::up, mTrans, 50.0f, 10.0f);
				return;
			}
		}
	}
}

void dae::AIComponent::RegisterComponent(BaseComponent * component)
{
	if (component->GetComponentID() == ComponentID::TransformComponent)
		mTrans = static_cast<TransformComponent*>(component);
}
