#pragma once
#include <vector>

#include "Common.h"
#include "Observer.h"

namespace dae
{
	class Subject
	{
	public:
		Subject() { mObservers.reserve(10); }

		void addObserver(Observer* observer)
		{
			mObservers.push_back(observer);
		}

		void removeObserver(Observer* observer)
		{
			for (size_t i = 0; i < mObservers.size(); ++i) {
				if (mObservers[i] && mObservers[i] == observer) {
					delete mObservers[i];
					mObservers[i] = nullptr;
				}
			}
		}

		void notify(unsigned int objectID, Event event)
		{
			for (auto o : mObservers)
				if (o) o->onNotify(objectID, event);
		}

	private:
		std::vector<Observer*> mObservers;
	};
}