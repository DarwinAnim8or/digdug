#pragma once
#include <string>
#include <fstream>

class BinaryWriter {
public:
	BinaryWriter(const std::string& filepath);
	~BinaryWriter();

	template<typename T>
	void WriteVariable(const T& value) {
		m_Outstream->write(reinterpret_cast<const char*>(&value), sizeof(T));
	}

	void WriteString(const std::string& str);

	const std::string GetOutpath() const { return m_Outpath; }
	const bool GetIsOkay();
	const size_t GetCurrentPosition();
	void Seek(size_t newPosition);

private:
	std::string m_Outpath;
	std::ofstream* m_Outstream;
};