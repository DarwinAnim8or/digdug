#pragma once
#include "Common.h"

namespace dae 
{
	class TransformComponent;

	class Command
	{
	public:
		virtual ~Command() {}
		virtual bool execute(float deltaTime) = 0;
		const bool IsCompleted() { return mIsCompleted; }

	protected:
		bool mIsCompleted;
	};

	class MoveCommand final : public Command
	{
	public:
		MoveCommand(dae::Direction dir, TransformComponent* trans, float velocity, float dist)
			: mDir(dir), mTrans(trans), mVelocity(velocity), mDistance(dist), Command() {}
		~MoveCommand() {}

		bool execute(float deltaTime);

	private:
		bool mHasRunOnce = false;
		float mVelocity;
		float mDistance;
		dae::Direction mDir;
		TransformComponent* mTrans;
		float mGoalX;
		float mGoalY;
	};

	class FirePumpCommand final : public Command
	{
	public:
		FirePumpCommand(dae::Direction dir, float x, float y) :
			mDir(dir), Command(), mX(x), mY(y) {}
		~FirePumpCommand() {}

		bool execute(float deltaTime);

	private:
		dae::Direction mDir;
		float mX;
		float mY;
	};
}