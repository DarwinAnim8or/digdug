#pragma once
#include "Common.h"
#include "Transform.h"
#include "SceneObject.h"
#include <map>

class BaseComponent;
class BaseMessage;

namespace dae
{
	class GameObject final : public SceneObject
	{
	public:
		void Update(float deltaTime) override;
		void Render() const override;

		const unsigned int GetObjectID();
		void AddComponent(BaseComponent* component);
		BaseComponent* GetComponent(ComponentID id);

		GameObject(unsigned int objectID);
		~GameObject();
		GameObject(const GameObject& other) = delete;
		GameObject(GameObject&& other) = delete;
		GameObject& operator=(const GameObject& other) = delete;
		GameObject& operator=(GameObject&& other) = delete;

	private:
		unsigned int mObjectID;
		std::map<ComponentID, BaseComponent*> mComponents;
	};
}
