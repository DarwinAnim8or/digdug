#pragma once
#include <XInput.h>
#include "Singleton.h"
#include <vector>

namespace dae
{
	enum class ControllerButton
	{
		ButtonA,
		ButtonB,
		ButtonX,
		ButtonY,
		TrackUp,
		TrackDown,
		TrackLeft,
		TrackRight
	};

	class InputManager final : public Singleton<InputManager>
	{
	public:
		bool ProcessInput();
		bool IsPressed(ControllerButton button) const;

	private:
		XINPUT_STATE currentState{};
		std::vector<ControllerButton> mButtonsThisFrame; 
	};

}
