#pragma once
#include "Common.h"
#include "Font.h"
#include <string>
#include <memory>

namespace dae
{
	class RenderComponent;

	class TextComponent : public BaseComponent
	{
	public:
		TextComponent() : BaseComponent(ComponentID::TextComponent) {}
		void Render();
		void Update(float deltaTime);
		void RegisterComponent(BaseComponent* component);

		void SetFont(const std::string& fontName, unsigned int size);
		void SetText(const std::string& text);

	private:
		bool mNeedsUpdate;
		std::string mText;
		std::shared_ptr<Font> mFont;
		RenderComponent* mRenderComponent;
	};
}