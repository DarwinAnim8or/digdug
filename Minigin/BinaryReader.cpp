#include "MiniginPCH.h"
#include "BinaryReader.h"
#include <iostream>

BinaryReader::BinaryReader(const std::string& outpath) : m_Outpath{ outpath }
{
	m_Instream = new std::ifstream();
	m_Instream->open(outpath, std::ios::in | std::ios::ate | std::ios::binary);
	if (m_Instream->is_open()) {
		std::cout << "Opened file: " << outpath << " for reading.\n";
		m_FileSize = size_t(m_Instream->tellg());
		m_Instream->seekg(0); //Seek back to our begin, as ::ate starts at the end.
	}
}

BinaryReader::~BinaryReader()
{
	if (m_Instream && m_Instream->is_open()) {
		std::cout << "Closing file: " << m_Outpath << ".\n";
		m_Instream->close();
		delete m_Instream;
		m_Instream = nullptr;
	}
}

std::string BinaryReader::ReadString()
{
	std::string toReturn;
	unsigned char stringSize{};
	ReadVariable(stringSize);

	char buffer;
	for (unsigned char i = 0; i < stringSize; ++i) {
		ReadVariable(buffer);
		toReturn += buffer;
	}

	return toReturn;
}

const bool BinaryReader::GetIsOkay()
{
	if (!m_Instream) return false;
	return m_Instream->is_open();
}

const size_t BinaryReader::GetCurrentPosition()
{
	if (!m_Instream) return size_t();
	return size_t(m_Instream->tellg());
}

void BinaryReader::Seek(size_t newPosition)
{
	if (!m_Instream) return;
	m_Instream->seekg(newPosition);
}
