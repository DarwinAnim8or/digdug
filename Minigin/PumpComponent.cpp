#include "MiniginPCH.h"
#include "PumpComponent.h"
#include "TransformComponent.h"
#include "Command.h"
#include "SceneManager.h"

dae::PumpComponent::~PumpComponent()
{
	if (mMove) 
	{
		delete mMove;
		mMove = nullptr;
	}
}

void dae::PumpComponent::Update(float deltaTime)
{
	if (mCurrentTimeAlive < mMaxTimeAlive)
	{
		mCurrentTimeAlive += deltaTime;
	}
	else
	{
		SceneManager::GetInstance().DestroyObject(mParentID);
		return;
	}

	if (mMove)
	{
		if (mMove->execute(deltaTime))
		{
			delete mMove;
			mMove = nullptr;
		}
	}
	else
	{
		SceneManager::GetInstance().DestroyObject(mParentID);
	}
}

void dae::PumpComponent::RegisterComponent(BaseComponent * component)
{
	if (component->GetComponentID() == ComponentID::TransformComponent)
		mTrans = static_cast<TransformComponent*>(component);
}
