#pragma once
#include "Common.h"

namespace dae
{
	class Observer
	{
	public:
		virtual ~Observer() {}
		virtual void onNotify(unsigned int objectID, Event event) = 0;
	};
}