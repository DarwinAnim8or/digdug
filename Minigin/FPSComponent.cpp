#include "MiniginPCH.h"
#include "FPSComponent.h"
#include "TextComponent.h"

void dae::FPSComponent::Update(float deltaTime)
{
	mFPS = int(std::round(1.0f / deltaTime));
	if (mFPS < 0) mFPS = 0;

	if (!mTextComponent) return;
	mTextComponent->SetText(std::to_string(mFPS) + " FPS");
}

void dae::FPSComponent::RegisterComponent(BaseComponent * component)
{
	if (component->GetComponentID() == ComponentID::TextComponent)
		mTextComponent = static_cast<TextComponent*>(component);
}
